<?php
namespace Bx\Properties;

/**
 * Реализация динамических свойств через ассоциативный массив.
 */
trait AssocProperties
{
    use Properties;
    
    /**
     * Массив значений динамических свойств.
     * @var array
     */
    protected $properties = [];

    /**
     * Возвращает значение динамического свойства, для которого не объявлен геттер.
     * @param  string $name Имя динамического свойства.
     * @return mixed        Значение свойства.
     */
    protected function getProperty(string $name)
    {
        return $this->properties[$name] ?? null;
    }

    /**
     * Задает значение динамического свойства, для которого не задан сеттер.
     * @param string $name  Имя динамического свойства.
     * @param mixed  $value Значение свойства.
     */
    protected function setProperty(string $name, $value)
    {
        $this->properties[$name] = $value;
    }
}
