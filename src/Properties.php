<?php
namespace Bx\Properties;

/**
 * Расширение, добавляющее классу поддержку динамических свойств.
 */
trait Properties
{
    /**
     * Возвращает значение динамического свойства, для которого не объявлен геттер.
     * @param  string $name Имя динамического свойства.
     * @return mixed        Значение свойства.
     */
    protected function getProperty(string $name)
    {
        return null;
    }

    /**
     * Задает значение динамического свойства, для которого не задан сеттер.
     * @param string $name  Имя динамического свойства.
     * @param mixed  $value Значение свойства.
     */
    protected function setProperty(string $name, $value) {}



    /**
     * Преобразует имя динамического свойства, к которому обратились как к переменной объекта.
     * @param  string $name Имя динамического свойства.
     * @return string       Новое имя динамического свойства.
     */
    protected function convertObjectPropertyName(string $name) : string
    {
        return $name;
    }

    /**
     * Преобразует имя динамического свойства, к которому обратились как к значению
     * ассоциативного массива.
     * @param  string $name Имя динамического свойства.
     * @return string       Новое имя динамического свойства.
     */
    protected function convertAssocPropertyName(string $name) : string
    {
        return $name;
    }



    /**
     * Возвращает имя метода геттера для указанного динамического свойства.
     * @param  string $name Имя динамического свойства.
     * @return string       Имя метода геттера.
     */
    protected function getPropertyGetterName(string $name) : string
    {
        $method = 'get'.ucfirst($name).'Property';
        return $method;
    }

    /**
     * Возвращает имя метода сеттера для указанного динамического свойства.
     * @param  string $name Имя динамического свойства.
     * @return string       Имя метода сеттера.
     */
    protected function getPropertySetterName(string $name) : string
    {
        $method = 'set'.ucfirst($name).'Property';
        return $method;
    }

    /**
     * Возвращает метод геттера для указанного динамического свойства.
     * @param  string $name Имя динамического свойства.
     * @return array        Вызываемый метод или null, если геттер не найден.
     */
    protected function getPropertyGetter(string $name) : ?array
    {
        $method = $this->getPropertyGetterName($name);
        return method_exists($this, $method) ? [$this, $method] : null;
    }

    /**
     * Возвращает метод сеттера для указанного динамического свойства.
     * @param  string $name Имя динамического свойства.
     * @return array        Вызываемый метод или null, если сеттер не найден.
     */
    protected function getPropertySetter(string $name) : ?array
    {
        $method = $this->getPropertySetterName($name);
        return method_exists($this, $method) ? [$this, $method] : null;
    }



    /**
     * Возвращает значение динамического свойства.
     * @param  string $name Имя свойства.
     * @return mixed        Значение свойства.
     */
    public function __get($name)
    {
        $name = $this->convertObjectPropertyName($name);
        $func = $this->getPropertyGetter($name);
        return $func ? call_user_func($func) : $this->getProperty($name);
    }

    /**
     * Задает значение динамического свойства.
     * @param string $name  Имя динамического свойства.
     * @param mixed  $value Значение.
     */
    public function __set($name, $value)
    {
        $name = $this->convertObjectPropertyName($name);
        $func = $this->getPropertySetter($name);
        $func ? call_user_func($func, $value) : $this->setProperty($name, $value);
    }

    /**
     * Возвращает true, если значение динамического свойства не равно null, иначе возвращает false.
     * @param  string $name Имя динамического свойства.
     * @return bool         True или false.
     */
    public function __isset($name)
    {
        $value = $this->$name;
        return isset($value);
    }

    /**
     * Удаляет значение динамического свойства.
     * @param string $name Имя динамического свойства.
     */
    public function __unset($name)
    {
        $this->$name = null;
    }



    /**
     * Возвращает значение динамического свойства.
     * @param string $name Имя динамического свойства.
     */
    public function offsetGet($name)
    {
        $name = $this->convertAssocPropertyName($name);
        $func = $this->getPropertyGetter($name);
        return $func ? call_user_func($func) : $this->getProperty($name);
    }

    /**
     * Задает значение динамического свойства.
     * @param string $name  Имя динамического свойства.
     * @param mixed  $value Значение.
     */
    public function offsetSet($name, $value)
    {
        $name = $this->convertAssocPropertyName($name);
        $func = $this->getPropertySetter($name);
        $func ? call_user_func($func, $value) : $this->setProperty($name, $value);
    }

    /**
     * Возвращает true, если значение динамического свойства не равно null, иначе возвращает false.
     * @param string $name Имя динамического свойства.
     */
    public function offsetExists($name)
    {
        $value = $this[$name];
        return isset($value);
    }

    /**
     * Удаляет значение динамического свойства.
     * @param string $name Имя динамического свойства.
     */
    public function offsetUnset($name)
    {
        $this[$name] = null;
    }
}
